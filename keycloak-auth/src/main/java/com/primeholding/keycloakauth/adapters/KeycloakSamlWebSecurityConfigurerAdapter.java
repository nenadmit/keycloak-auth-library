package com.primeholding.keycloakauth.adapters;

import com.primeholding.keycloakauth.properties.SamlProperties;
import com.primeholding.keycloakauth.properties.SamlPropertiesImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.extensions.saml2.config.SAMLConfigurer;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import com.primeholding.keycloakauth.service.SAMLUserService;

import static org.springframework.security.extensions.saml2.config.SAMLConfigurer.saml;

public class KeycloakSamlWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Bean
    protected SamlProperties samlProperties(){
        return new SamlPropertiesImpl();
    }

    protected  SAMLUserDetailsService samlUserService(){
        return new SAMLUserService();
    }

    protected SAMLConfigurer samlConfigurer(){

        SamlProperties samlProperties = samlProperties();
        if(samlProperties == null)
            throw new IllegalStateException("Saml Configuration properties cannot be null");

        SAMLConfigurer samlConfigurer = saml();
        samlConfigurer.userDetailsService(samlUserService());
        samlConfigurer.identityProvider().metadataFilePath(samlProperties.getMetadata());
        samlConfigurer.serviceProvider()
                .entityId(samlProperties.getEntityId())
                .basePath(samlProperties.getBasePath())
                .protocol(samlProperties.getProtocol())
                .hostname(samlProperties.getHost())
                .keyStore()
                .storeFilePath(samlProperties.getKeystoreFilePath())
                .keyname(samlProperties.getKeystoreAlias())
                .keyPassword(samlProperties.getKeystorePassword());

        return samlConfigurer;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/saml/**").permitAll();
        http.apply(samlConfigurer());

    }
}
