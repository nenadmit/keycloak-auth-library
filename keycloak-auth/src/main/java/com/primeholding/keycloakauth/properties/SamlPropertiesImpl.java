package com.primeholding.keycloakauth.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "saml-config")
public class SamlPropertiesImpl implements SamlProperties{

    String metadata;
    String keystoreFilePath;
    String keystoreAlias;
    String keystorePassword;
    String host;
    String protocol;
    String basePath;
    String entityId;
}
