package com.primeholding.keycloakauth.properties;

public interface SamlProperties {

    String getMetadata();
    String getKeystoreFilePath();
    String getKeystoreAlias();
    String getKeystorePassword();
    String getEntityId();
    String getProtocol();
    String getHost();
    String getBasePath();
}
