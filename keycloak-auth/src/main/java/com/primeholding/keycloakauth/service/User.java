package com.primeholding.keycloakauth.service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    private String id;

    public User(String id) {
        this.id = id;
    }
}
