# Keycloak - Spring Boot authentication

Package consists of two WebSecurityConfigurerAdapter classess which can you used to implement OAuth and Saml 
authentication with Spring Boot Security and Keycloak.

Oauth authentication with openid-connect:

1.Create a @Configuration class which extends KeycloakOauthWebSecurityConfigurerAdapter.
2.Create a keycloak client with a openid-connect protocol and select an authentication flow.
3.Set the configuration in application.yml
```
keycloak:
  auth-server-url: http://localhost:8080/auth
  realm: {REALM_NAME}
  resource: {CLIENT_ID}
  bearer-only: true
  principal-attribute: preferred_username
```

Saml authentication:

1. Create a @Configuration class which extends KeycloakSamlWebSecurityConfigurerAdapter
2. Create a Keycloak client with saml client protocol
3. Set saml configuration in application.yml
```
saml-config:
  entity-id: {CLIENT_ID}
  host: {HOST (example localhost:8085)}
  base-path: /
  protocol: http
  metadata: {KEYCLOAK_URL}/auth/realms/demo/protocol/saml/descriptor
  keystore-alias: {KEYSTORE_ALIAS}
  keystore-password: {KEYSTORE_PASS}
  keystore-file-path: {KEYSTORE_FILE_PATH}
```

