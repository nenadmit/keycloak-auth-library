package com.primeholding.testapp;

import com.primeholding.keycloakauth.adapters.KeycloakSamlWebSecurityConfigurerAdapter;
import com.primeholding.keycloakauth.properties.SamlProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;


@Configuration
public class WebConfig extends KeycloakSamlWebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.authorizeRequests().anyRequest().authenticated();
    }
    @Bean
    @Override
    protected SamlProperties samlProperties() {
        return new SamlPropertiess();
    }
}
