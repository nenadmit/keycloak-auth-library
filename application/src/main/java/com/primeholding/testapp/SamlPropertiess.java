package com.primeholding.testapp;

import com.primeholding.keycloakauth.properties.SamlProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "saml-config")
@Getter
@Setter
@ToString
public class SamlPropertiess implements SamlProperties {

    String metadata;
    String keystoreFilePath;
    String keystoreAlias;
    String keystorePassword;
    String host;
    String protocol;
    String basePath;
    String entityId;
}
